CREATE DATABASE EquiposFut;
USE EquiposFut;

CREATE TABLE Posicion(
	Id INT AUTO_INCREMENT,
    Posicion VARCHAR(50),
    
    CONSTRAINT pk_Posicion PRIMARY KEY (Id)
);

CREATE TABLE Equipo(
	Id INT AUTO_INCREMENT,
    Nombre VARCHAR(100),
    UniformeVisitante VARCHAR(150),
    UniformeLocal VARCHAR(150),
    CantidadJugadores INT,
    NombreEstadio VARCHAR(100),
    Localidad VARCHAR(100),
    
    CONSTRAINT pk_Equipo PRIMARY KEY (Id)    
);

CREATE TABLE Jugador(
	Id INT AUTO_INCREMENT,
    NombreJugador VARCHAR(80),
    ApellidoPaterno VARCHAR(80),
    ApellidoMaterno VARCHAR(80),
    Edad INT DEFAULT 25, 
    Posicion INT NOT NULL,
    Equipo INT NOT NULL,
    
    CONSTRAINT pk_jugador PRIMARY KEY (Id),
    CONSTRAINT fk_jugador_equipo FOREIGN KEY (Equipo) REFERENCES Equipo (id),
    CONSTRAINT fk_jugador_posicion FOREIGN KEY (Posicion) REFERENCES Posicion (id)
);


INSERT INTO Posicion (Posicion) VALUES ('Portero'),('Defensa'),('Medio'),('Delantero');
        
INSERT INTO Equipo (Nombre, UniformeVisitante, UniformeLocal, CantidadJugadores, NombreEstadio, Localidad) 
VALUES ('América', 'Azul/Amarillo','Amarrillo',25,'Azteca','CDMX'), 
		('Monterrey', 'Azul','Azul/Blanco',26,'BBVA','Nuevo León'), 
        ('Toluca', 'Blanco','Rojo',23,'Nemesio Diez','Toluca');


INSERT INTO Jugador (NombreJugador, ApellidoPaterno, ApellidoMaterno, Edad, Posicion, Equipo) 
VALUES ('Carlos','Lopez','Fernández', 25,3,2),
		('Fernando','Pérez','Gonzáles',30,2,1),
        ('Javier','Oropeza','Vargas',28,2,3);

CREATE VIEW vw_jugadores
AS
	SELECT J.Id, J.NombreJugador, J.ApellidoPaterno, J.ApellidoMaterno, J.Edad, P.Id AS IdPos, P.Posicion, E.Id AS IdEquipo, E.Nombre AS Equipo
	   FROM Equipo AS E INNER JOIN Jugador AS J ON E.Id = J.Equipo
       INNER JOIN Posicion AS P ON J.Posicion = P.Id;
       

SELECT * FROM vw_jugadores
SELECT * FROM Posicion