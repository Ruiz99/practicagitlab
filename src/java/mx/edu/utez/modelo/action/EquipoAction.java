/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utez.modelo.action;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.List;
import mx.edu.utez.modelo.bean.BeanEquipo;
import mx.edu.utez.modelo.dao.DaoEquipo;

/**
 *
 * @author CDS
 */
public class EquipoAction extends ActionSupport {

    private DaoEquipo dao = new DaoEquipo();
    private BeanEquipo equipoR = new BeanEquipo();
    private BeanEquipo equipoM = new BeanEquipo();

    private List<BeanEquipo> equipos = new ArrayList<BeanEquipo>();

    private String parametros, respuesta;

    private int idM, idE;

    public String eliminarEquipo() {
        idE = new Gson().fromJson(parametros, Integer.class);
        if (dao.eliminarEquipo(idE)) {
            respuesta = "El equipo ha sido eliminado del sistema";
            return SUCCESS;
        } else {
            respuesta = "No hubo cambios";
            return ERROR;
        }
    }

    public String registrarEquipo() {
        equipoR = new Gson().fromJson(parametros, BeanEquipo.class);
        if (dao.registrarEquipo(equipoR)) {
            respuesta = "Equipo registrado en el sistema";
        } else {
            respuesta = "El equipo no se pudo registrar.";
        }
        return SUCCESS;
    }

    public String consultarEquipo() {
        idM = new Gson().fromJson(parametros, Integer.class);
        equipoM = dao.consultarEquipo(idM);
        return SUCCESS;
    }

    public String modificarEquipo() {
        equipoM = new Gson().fromJson(parametros, BeanEquipo.class);
        if (dao.modificarEquipo(equipoM)) {
            respuesta = "Equipo modificado";
        } else {
            respuesta = "No hubo cambios";
        }
        return SUCCESS;
    }

    public String consultarEquipos() {
        try {
            equipos = dao.consultaEquipos();
            return "success";
        } catch (Exception e) {
            System.out.println("Error -> " + e);
            return "error";
        }
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    public BeanEquipo getEquipoR() {
        return equipoR;
    }

    public void setEquipoR(BeanEquipo equipoR) {
        this.equipoR = equipoR;
    }

    public BeanEquipo getEquipoM() {
        return equipoM;
    }

    public void setEquipoM(BeanEquipo equipoM) {
        this.equipoM = equipoM;
    }

    public List<BeanEquipo> getEquipos() {
        return equipos;
    }

    public void setEquipos(List<BeanEquipo> equipos) {
        this.equipos = equipos;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public int getIdE() {
        return idE;
    }

    public void setIdE(int idE) {
        this.idE = idE;
    }

    public int getIdM() {
        return idM;
    }

    public void setIdM(int idM) {
        this.idM = idM;
    }

}
