/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utez.modelo.action;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.List;
import mx.edu.utez.modelo.bean.BeanEquipo;
import mx.edu.utez.modelo.bean.BeanJugador;
import mx.edu.utez.modelo.bean.BeanPosicion;
import mx.edu.utez.modelo.dao.DaoEquipo;
import mx.edu.utez.modelo.dao.DaoJugador;
import mx.edu.utez.modelo.dao.DaoPosicion;

/**
 *
 * @author CDS
 */
public class JugadorAction extends ActionSupport {

    private DaoJugador dao = new DaoJugador();
    private DaoEquipo daoE = new DaoEquipo();
    private DaoPosicion daoP = new DaoPosicion();

    private BeanJugador jugadorR = new BeanJugador();
    private BeanJugador jugadorM = new BeanJugador();

    private List<BeanJugador> jugadores = new ArrayList<BeanJugador>();
    private List<BeanPosicion> posiciones = new ArrayList<BeanPosicion>();
    private List<BeanEquipo> equipos = new ArrayList<BeanEquipo>();

    private String parametros, respuesta;

    private int idM, idE;

    public String eliminarJugador() {
        idE = new Gson().fromJson(parametros, Integer.class);
        if (dao.eliminarJugador(idE)) {
            respuesta = "El jugador ha sido eliminado del sistema";            
        } else {
            respuesta = "No hubo cambios";
        }
        return SUCCESS;
    }

    public String registrarJugador() {
        jugadorR = new Gson().fromJson(parametros, BeanJugador.class);
        if (dao.registrarJugador(jugadorR)) {
            respuesta = "Jugador registrado en el sistema";
        } else {
            respuesta = "El jugador no se pudo registrar.";
        }
        return SUCCESS;
    }

    public String consultarJugador() {
        idM = new Gson().fromJson(parametros, Integer.class);
        jugadorM = dao.consultarJugador(idM);
        return SUCCESS;
    }

    public String modificarJugador() {
        jugadorM = new Gson().fromJson(parametros, BeanJugador.class);
        if (dao.modificarJugador(jugadorM)) {
            respuesta = "Jugador modificado";
        } else {
            respuesta = "No hubo cambios";
        }
        return SUCCESS;
    }

    public String consultarJugadores() {
        jugadores = dao.consultaJugadores();
        posiciones = daoP.consultaPosiciones();
        equipos = daoE.consultaEquipos();
        posiciones.add(new BeanPosicion(0, "Seleccione..."));
        equipos.add(new BeanEquipo(0, "Seleccione..."));
        return SUCCESS;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public int getIdE() {
        return idE;
    }

    public void setIdE(int idE) {
        this.idE = idE;
    }

    public int getIdM() {
        return idM;
    }

    public void setIdM(int idM) {
        this.idM = idM;
    }

    public BeanJugador getJugadorR() {
        return jugadorR;
    }

    public void setJugadorR(BeanJugador jugadorR) {
        this.jugadorR = jugadorR;
    }

    public BeanJugador getJugadorM() {
        return jugadorM;
    }

    public void setJugadorM(BeanJugador jugadorM) {
        this.jugadorM = jugadorM;
    }

    public List<BeanJugador> getJugadores() {
        return jugadores;
    }

    public void setJugadores(List<BeanJugador> jugadores) {
        this.jugadores = jugadores;
    }

    public List<BeanPosicion> getPosiciones() {
        return posiciones;
    }

    public void setPosiciones(List<BeanPosicion> posiciones) {
        this.posiciones = posiciones;
    }

    public List<BeanEquipo> getEquipos() {
        return equipos;
    }

    public void setEquipos(List<BeanEquipo> equipos) {
        this.equipos = equipos;
    }

}
