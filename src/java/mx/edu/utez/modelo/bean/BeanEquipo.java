/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utez.modelo.bean;

/**
 *
 * @author CDS
 */
public class BeanEquipo {

    private int id, cantJugadores;
    private String nombre, uniV, uniL, estadio, ciudad;

    public BeanEquipo() {
    }

    public BeanEquipo(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    
    public BeanEquipo(int id, String nombre, String uniV, String uniL, int cantJugadores, String estadio, String ciudad) {
        this.id = id;
        this.cantJugadores = cantJugadores;
        this.nombre = nombre;
        this.uniV = uniV;
        this.uniL = uniL;
        this.estadio = estadio;
        this.ciudad = ciudad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCantJugadores() {
        return cantJugadores;
    }

    public void setCantJugadores(int cantJugadores) {
        this.cantJugadores = cantJugadores;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUniV() {
        return uniV;
    }

    public void setUniV(String uniV) {
        this.uniV = uniV;
    }

    public String getUniL() {
        return uniL;
    }

    public void setUniL(String uniL) {
        this.uniL = uniL;
    }

    public String getEstadio() {
        return estadio;
    }

    public void setEstadio(String estadio) {
        this.estadio = estadio;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}
