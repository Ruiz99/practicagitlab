/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utez.modelo.bean;

/**
 *
 * @author CDS
 */
public class BeanJugador {

    private int id, edad;
    private String nombre, apeP, apeM;
    private BeanPosicion posicion;
    private BeanEquipo equipo;

    public BeanJugador() {
    }

    public BeanJugador(int id, String nombre, String apeP, String apeM, int edad, BeanPosicion posicion, BeanEquipo equipo) {
        this.id = id;
        this.edad = edad;
        this.nombre = nombre;
        this.apeP = apeP;
        this.apeM = apeM;
        this.posicion = posicion;
        this.equipo = equipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApeP() {
        return apeP;
    }

    public void setApeP(String apeP) {
        this.apeP = apeP;
    }

    public String getApeM() {
        return apeM;
    }

    public void setApeM(String apeM) {
        this.apeM = apeM;
    }

    public BeanPosicion getPosicion() {
        return posicion;
    }

    public void setPosicion(BeanPosicion posicion) {
        this.posicion = posicion;
    }

    public BeanEquipo getEquipo() {
        return equipo;
    }

    public void setEquipo(BeanEquipo equipo) {
        this.equipo = equipo;
    }

}
