/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utez.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import mx.edu.utez.modelo.bean.BeanEquipo;
import mx.edu.utez.modelo.utilerias.ConexionMySQL;

/**
 *
 * @author CDS
 */
public class DaoEquipo {

    Connection conexion = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    private final String sqlConsultaGeneral = "SELECT * FROM Equipo;";

    public List<BeanEquipo> consultaEquipos() {
        List<BeanEquipo> equipos = new ArrayList<>();
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlConsultaGeneral);
            rs = ps.executeQuery();
            while (rs.next()) {
                BeanEquipo equipo = new BeanEquipo(rs.getInt("Id"), rs.getString("Nombre"), rs.getString("UniformeVisitante"), rs.getString("UniformeLocal"), rs.getInt("CantidadJugadores"), rs.getString("NombreEstadio"), rs.getString("Localidad"));
                equipos.add(equipo);
            }
        } catch (Exception e) {
            System.err.println("Error -> " + e.getMessage());
        } finally {
            cerrarFlujos("ConsultaEquipos");
        }
        return equipos;
    }

    private final String sqlConsultaEquipo = "SELECT * FROM Equipo WHERE Id = ?;";

    public BeanEquipo consultarEquipo(int id) {
        BeanEquipo equipoConsultado = null;
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlConsultaEquipo);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                equipoConsultado = new BeanEquipo(rs.getInt("Id"), rs.getString("Nombre"), rs.getString("UniformeVisitante"), rs.getString("UniformeLocal"), rs.getInt("CantidadJugadores"), rs.getString("NombreEstadio"), rs.getString("Localidad"));
            }
        } catch (Exception e) {
            System.err.println("Error -> " + e.getMessage());
        } finally {
            cerrarFlujos("ConsultarEquipo");
        }
        return equipoConsultado;
    }

    private final String sqlRegistrarEquipo = "INSERT INTO Equipo (Nombre, UniformeVisitante, UniformeLocal, CantidadJugadores, NombreEstadio, Localidad) VALUES (?,?,?,?,?,?);";

    public boolean registrarEquipo(BeanEquipo equipo) {
        boolean resultado = false;
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlRegistrarEquipo, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, equipo.getNombre());
            ps.setString(2, equipo.getUniV());
            ps.setString(3, equipo.getUniL());
            ps.setInt(4, equipo.getCantJugadores());
            ps.setString(5, equipo.getEstadio());
            ps.setString(6, equipo.getCiudad());
            resultado = ps.executeUpdate() == 1;
        } catch (Exception e) {
            System.err.println("Error -> " + e.getMessage());
        } finally {
            cerrarFlujos("RegistrarEquipo");
        }
        return resultado;
    }

    private final String sqlModificarEquipo = "UPDATE Equipo SET Nombre = ?, UniformeVisitante = ?, UniformeLocal = ?, CantidadJugadores = ?, NombreEstadio = ?, Localidad = ? WHERE Id = ?;";

    public boolean modificarEquipo(BeanEquipo equipo) {
        boolean modificado = false;
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlModificarEquipo);
            ps.setString(1, equipo.getNombre());
            ps.setString(2, equipo.getUniV());
            ps.setString(3, equipo.getUniL());
            ps.setInt(4, equipo.getCantJugadores());
            ps.setString(5, equipo.getEstadio());
            ps.setString(6, equipo.getCiudad());
            ps.setInt(7, equipo.getId());
            modificado = ps.executeUpdate() == 1;
        } catch (Exception e) {
            System.err.println("Error en el método sql -> " + e.getMessage());
        } finally {
            cerrarFlujos("modificarEquipo");
        }
        return modificado;
    }

    private final String sqlEliminarEquipo = "DELETE FROM Equipo WHERE Id = ?;";

    public boolean eliminarEquipo(int id) {
        boolean eliminado = false;
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlEliminarEquipo);
            ps.setInt(1, id);
            eliminado = ps.executeUpdate() == 1;
        } catch (Exception e) {
            System.err.println("Error en el método sql -> " + e.getMessage());
        } finally {
            cerrarFlujos("eliminarEquipo");
        }
        return eliminado;
    }

    private void cerrarFlujos(String metodo) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conexion != null) {
                conexion.close();
            }
        } catch (Exception e) {
            System.err.println("Error al cerrar conexiones - DaoCuatrimestre - " + metodo + "->" + e.getMessage());
        }
    }
}
