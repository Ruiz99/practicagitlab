/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utez.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import mx.edu.utez.modelo.bean.BeanEquipo;
import mx.edu.utez.modelo.bean.BeanJugador;
import mx.edu.utez.modelo.bean.BeanPosicion;
import mx.edu.utez.modelo.utilerias.ConexionMySQL;

/**
 *
 * @author CDS
 */
public class DaoJugador {

    Connection conexion = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    private final String sqlConsultaGeneral = "SELECT * FROM vw_jugadores;";

    public List<BeanJugador> consultaJugadores() {
        List<BeanJugador> jugadores = new ArrayList<>();
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlConsultaGeneral);
            rs = ps.executeQuery();
            while (rs.next()) {
                BeanJugador jugador = new BeanJugador(rs.getInt("Id"), rs.getString("NombreJugador"), rs.getString("ApellidoPaterno"), rs.getString("ApellidoMaterno"), rs.getInt("Edad"), new BeanPosicion(rs.getInt("IdPos"), rs.getString("Posicion")), new BeanEquipo(rs.getInt("idEquipo"), rs.getString("Equipo")));
                jugadores.add(jugador);
            }
        } catch (Exception e) {
            System.err.println("Error -> " + e.getMessage());
        } finally {
            cerrarFlujos("ConsultaJugadores");
        }
        return jugadores;
    }

    private final String sqlConsultaJugador = "SELECT * FROM vw_jugadores WHERE Id = ?;";

    public BeanJugador consultarJugador(int id) {
        BeanJugador jugadorConsultado = null;
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlConsultaJugador);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                jugadorConsultado = new BeanJugador(rs.getInt("Id"), rs.getString("NombreJugador"), rs.getString("ApellidoPaterno"), rs.getString("ApellidoMaterno"), rs.getInt("Edad"), new BeanPosicion(rs.getInt("IdPos"), rs.getString("Posicion")), new BeanEquipo(rs.getInt("idEquipo"), rs.getString("Equipo")));
            }
        } catch (Exception e) {
            System.err.println("Error -> " + e.getMessage());
        } finally {
            cerrarFlujos("ConsultarJugador");
        }
        return jugadorConsultado;
    }

    private final String sqlRegistrarJugador = "INSERT INTO Jugador (NombreJugador, ApellidoPaterno, ApellidoMaterno, Edad, Posicion, Equipo) VALUES (?,?,?,?,?,?);";

    public boolean registrarJugador(BeanJugador jugador) {
        boolean resultado = false;
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlRegistrarJugador);
            ps.setString(1, jugador.getNombre());
            ps.setString(2, jugador.getApeP());
            ps.setString(3, jugador.getApeM());
            ps.setInt(4, jugador.getEdad());
            ps.setInt(5, jugador.getPosicion().getId());
            ps.setInt(6, jugador.getEquipo().getId());
            resultado = ps.executeUpdate() == 1;
        } catch (Exception e) {
            System.err.println("Error -> " + e.getMessage());
        } finally {
            cerrarFlujos("RegistrarJugador");
        }
        return resultado;
    }

    private final String sqlModificarEquipo = "UPDATE Jugador SET NombreJugador = ?, ApellidoPaterno = ?, ApellidoMaterno = ?, Edad = ?, Posicion = ?, Equipo = ? WHERE Id = ?;";

    public boolean modificarJugador(BeanJugador jugador) {
        boolean modificado = false;
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlModificarEquipo);
            ps.setString(1, jugador.getNombre());
            ps.setString(2, jugador.getApeP());
            ps.setString(3, jugador.getApeM());
            ps.setInt(4, jugador.getEdad());
            ps.setInt(5, jugador.getPosicion().getId());
            ps.setInt(6, jugador.getEquipo().getId());
            ps.setInt(7, jugador.getId());
            modificado = ps.executeUpdate() == 1;
        } catch (Exception e) {
            System.err.println("Error en el método sql -> " + e.getMessage());
        } finally {
            cerrarFlujos("modificarJugador");
        }
        return modificado;
    }

    private final String sqlEliminarJugador = "DELETE FROM Jugador WHERE Id = ?;";

    public boolean eliminarJugador(int id) {
        boolean eliminado = false;
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlEliminarJugador);
            ps.setInt(1, id);
            eliminado = ps.executeUpdate() == 1;
        } catch (Exception e) {
            System.err.println("Error en el método sql -> " + e.getMessage());
        } finally {
            cerrarFlujos("eliminarJugador");
        }
        return eliminado;
    }

    private void cerrarFlujos(String metodo) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conexion != null) {
                conexion.close();
            }
        } catch (Exception e) {
            System.err.println("Error al cerrar conexiones - DaoCuatrimestre - " + metodo + "->" + e.getMessage());
        }
    }
}
