/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utez.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import mx.edu.utez.modelo.bean.BeanPosicion;
import mx.edu.utez.modelo.utilerias.ConexionMySQL;

/**
 *
 * @author CDS
 */
public class DaoPosicion {

    Connection conexion = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    private final String sqlConsultaGeneral = "SELECT * FROM Posicion;";

    public List<BeanPosicion> consultaPosiciones() {
        List<BeanPosicion> posiciones = new ArrayList<>();
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlConsultaGeneral);
            rs = ps.executeQuery();
            while (rs.next()) {
                BeanPosicion posicion = new BeanPosicion(rs.getInt("Id"), rs.getString("Posicion"));
                posiciones.add(posicion);
            }
        } catch (Exception e) {
            System.err.println("Error -> " + e.getMessage());
        } finally {
            cerrarFlujos("ConsultaPosiciones");
        }
        return posiciones;
    }

    private void cerrarFlujos(String metodo) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conexion != null) {
                conexion.close();
            }
        } catch (Exception e) {
            System.err.println("Error al cerrar conexiones - DaoCuatrimestre - " + metodo + "->" + e.getMessage());
        }
    }
}
