<%-- 
    Document   : index
    Created on : 16/05/2019, 09:35:52 AM
    Author     : CDS
--%>
<%
    String context = request.getContextPath();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html ng-app="crudAngular">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CRUD</title>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link rel="icon" type="image/png" href="https://image.flaticon.com/icons/png/512/55/55832.png">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    </head>
    <body>
        <div class="jumbotron jumbotron-fluid">
            <center><h1>Fútbol</h1></center>
        </div>
        <div class="container-fluid">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link" id="nav-equipos-tab" data-toggle="tab" ng-href="index#!/equipo" role="tab" aria-controls="nav-equipos" aria-selected="true">Equipos</a>
                    <a class="nav-item nav-link" id="nav-jugadores-tab" data-toggle="tab" ng-href="index#!/jugador" role="tab" aria-controls="nav-jugadores" aria-selected="false">Jugadores</a>
                </div>
            </nav>
            <br>
            <div ng-view ></div>
        </div>
        <script rel="javascript" type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://code.angularjs.org/1.7.8/angular-route.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <script src="<%=context%>/js/acciones.js"></script>
    </body>
</html>
