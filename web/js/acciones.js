var raiz = window.location.origin + '/GitLab/';


var app = angular.module('crudAngular', ['ngRoute'])

app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
                when('/equipo', {
                    templateUrl: 'plantillas/equipos.html',
                }).
                when('/jugador', {
                    templateUrl: 'plantillas/jugadores.html',
                }).
                otherwise({
                    redirectTo: '/',
                    templateUrl: 'plantillas/inicio.html',
                });
    }]);

app.controller('controladorEquipo', ['$scope', '$http', function ($scope, $http) {

        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        $scope.registrado = {};
        $scope.modificado = {};
        $scope.inicio = function () {
            $('#formRegistro').hide();
            $('#formModificar').hide();

            $http.post(raiz + 'consultarEquipos', {}).then(function (exitoso) {
                $scope.equipos = exitoso.data.equipos;
            }, function (error) {});
        }

        $scope.btnAgregar = function () {
            $scope.registrado.nombre = "";
            $scope.registrado.cantJugadores = "";
            $scope.registrado.uniV = "";
            $scope.registrado.uniL = "";
            $scope.registrado.estadio = "";
            $scope.registrado.ciudad = "";
            $('#listaEquipos').hide();
            $('#formModificar').hide();
            $('#formRegistro').fadeIn(1000);
        }

        $scope.btnReg = function () {
            Swal.fire({
                title: 'Registro completo',
                text: "Se registrará a: " + $scope.registrado.nombre,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(result => {
                if (result.value) {
                    $('#formModificar').hide();
                    $('#formRegistro').hide();
                    $('#listaEquipos').fadeIn(1000);
                    $http.post(raiz + 'registrarEquipo', "parametros=" + angular.toJson($scope.registrado)).then(function (exitoso) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: exitoso.data.respuesta,
                            showConfirmButton: false,
                            timer: 2000
                        });
                        $scope.inicio();
                    }, function (error) {});
                }
            });
        }

        $scope.btnMod = function () {
            Swal.fire({
                title: 'Modificación completa',
                text: "Se modificará a: " + $scope.modificado.nombre,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
            }).then((result) => {
                if (result.value) {
                    $('#formModificar').hide();
                    $('#formRegistro').hide();
                    $('#listaEquipos').fadeIn(1000);
                    $http.post(raiz + 'modificarEquipo', "parametros=" + angular.toJson($scope.modificado)).then(function (exitoso) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: exitoso.data.respuesta,
                            showConfirmButton: false,
                            timer: 2000
                        });
                        $scope.inicio();
                    }, function (error) {});
                }
            });
        }

        $scope.btnConsultar = function () {
            $('#formRegistro').hide();
            $('#formModificar').hide();
            $('#listaEquipos').fadeIn(1000);
        }

        $scope.btnRegresar = function () {
            $('#formRegistro').hide();
            $('#formModificar').hide();
            $('#listaEquipos').fadeIn(1000);
        }

        $scope.consultaAMod = function (bean) {
            $('#listaEquipos').hide();
            $('#formRegistro').hide();
            $('#formModificar').fadeIn(1000);
            angular.copy(bean, $scope.modificado);
        }

        $scope.eliminar = function (e) {
            var id = e.currentTarget.id;
            Swal.fire({
                title: 'Confirmación de elimición',
                text: "Se eliminará a un equipo del sistema",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
            }).then((result) => {
                if (result.value) {
                    $http.post(raiz + 'eliminarEquipo', "parametros=" + angular.toJson(id)).then(function (exitoso) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: exitoso.data.respuesta,
                            showConfirmButton: false,
                            timer: 2000
                        });
                        $scope.inicio();
                    }, function (error) {
                        console.log(error)
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'No se puede eliminar por que tiene jugadores registrados',
                            showConfirmButton: false,
                            timer: 2000
                        });
                    });
                }
            });
        }
    }])

app.controller('controladorJugador', ['$scope', '$http', function ($scope, $http) {

        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        $scope.jugadorR = {};
        $scope.jugadorM = {};
        $scope.inicio2 = function () {
            $('#formRegistro').hide();
            $('#formModificar').hide();
            $http.post(raiz + 'consultarJugadores', {}).then(function (exitoso) {
                $scope.jugadores = exitoso.data.jugadores;
                $scope.equipos = exitoso.data.equipos;
                $scope.posiciones = exitoso.data.posiciones;
            }, function (error) {});
        }

        $scope.btnAgregarJ = function () {
            $scope.jugadorR.nombre = "";
            $scope.jugadorR.cantJugadores = "";
            $scope.jugadorR.uniV = "";
            $scope.jugadorR.uniL = "";
            $scope.jugadorR.estadio = "";
            $scope.jugadorR.ciudad = "";
            $('#listaJugadores').hide();
            $('#formModificar').hide();
            $('#formRegistro').fadeIn(1000);
        }

        $scope.btnReg = function () {
            Swal.fire({
                title: 'Registro completo',
                text: "Se registrará a: " + $scope.jugadorR.nombre,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(result => {
                if (result.value) {
                    $('#formModificar').hide();
                    $('#formRegistro').hide();
                    $('#listaJugadores').fadeIn(1000);
                    $http.post(raiz + 'registrarJugador', "parametros=" + angular.toJson($scope.jugadorR)).then(function (exitoso) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: exitoso.data.respuesta,
                            showConfirmButton: false,
                            timer: 2000
                        });
                        $scope.inicio2();
                    }, function (error) {});
                }
            });
        }

        $scope.btnMod = function () {
            Swal.fire({
                title: 'Modificación completa',
                text: "Se modificará a: " + $scope.jugadorM.nombre,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
            }).then((result) => {
                if (result.value) {
                    $('#formModificar').hide();
                    $('#formRegistro').hide();
                    $('#listaJugadores').fadeIn(1000);
                    $http.post(raiz + 'modificarJugador', "parametros=" + angular.toJson($scope.jugadorM)).then(function (exitoso) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: exitoso.data.respuesta,
                            showConfirmButton: false,
                            timer: 2000
                        });
                        $scope.inicio2();
                    }, function (error) {});
                }
            });
        }

        $scope.btnJugadores = function () {
            $('#formRegistro').hide();
            $('#formModificar').hide();
            $('#listaJugadores').fadeIn(1000);
        }

        $scope.btnRegresar = function () {
            $('#formRegistro').hide();
            $('#formModificar').hide();
            $('#listaJugadores').fadeIn(1000);
        }

        $scope.consultaAMod = function (bean) {
            $('#listaJugadores').hide();
            $('#formRegistro').hide();
            $('#formModificar').fadeIn(1000);
            angular.copy(bean, $scope.jugadorM);
            console.log($scope.jugadorM)
        }

        $scope.eliminar = function (e) {
            var id = e.currentTarget.id;
            Swal.fire({
                title: 'Confirmación de elimición',
                text: "Se eliminará a un jugador del sistema",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
            }).then((result) => {
                if (result.value) {
                    $http.post(raiz + 'eliminarJugador', "parametros=" + angular.toJson(id)).then(function (exitoso) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: exitoso.data.respuesta,
                            showConfirmButton: false,
                            timer: 2000
                        });
                        $scope.inicio2();
                    }, function (error) {});
                }
            });
        }
    }])